module git.devnet.page/Delorus/golang_gitlab_ci

go 1.16

require (
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/vektra/mockery/v2 v2.9.0 // indirect
)
