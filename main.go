package main

import (
	"fmt"
)

func main() {
	fmt.Printf("hello world\n")
}

//go:generate mockery --inpackage -r --case snake --name ConnInterface
type ConnInterface interface {
	Send(msg *Msg) error
	RemoveHandlerSubject(subject Subject) bool
	Close()
	IsClosed() bool
	IsConnected() bool
	IsReconnecting() bool
	SubscribeToGroup(group, peer string) error
	SubscribeSelf(group string) error
	UnsubscribeFromGroup(group, peer string) error
	UnsubscribeSelf(group string) error
}

type Msg struct {
	Proto      Protocol
	Subject    Subject
	From       string
	To         string
	ID         string
	Error      string
	Data       []byte
	Duplicated bool
}

type Protocol interface {
	// Decode Преобразовывает данные из формата протокола в Msg
	Decode([]byte) (*Msg, error)

	// Encode Преобразовывает Msg в формат протокола
	Encode(*Msg) ([]byte, error)
}

type Subject struct {
	Name string
	Type string
}

func Foo() {
	fmt.Printf("fooooo")
}

//func Bar() {
//	fmt.Printf("baaaaaaar")
//}
//
//type HelloWorld struct {
//	NeverUser string
//}
//
//type HelloWorl2 struct {
//	NeverUser string
//}
//
//var UnusedLol = "hello"
